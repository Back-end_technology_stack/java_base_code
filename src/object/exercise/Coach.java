package object.exercise;

public abstract class Coach extends Person{
    public Coach() {
    }

    public Coach(String name, String age) {
        super(name, age);
    }

    public abstract void teach();
}
