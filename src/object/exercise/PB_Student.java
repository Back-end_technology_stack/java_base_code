package object.exercise;

public class PB_Student extends Student implements English{
    public PB_Student() {
    }

    public PB_Student(String name, String age) {
        super(name, age);
    }

    @Override
    public void sai_en() {
        System.out.println("乒乓球学员说英语");
    }

    @Override
    public void study() {
        System.out.println("乒乓球学员学打乒乓球");
    }
}
