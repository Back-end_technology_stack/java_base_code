package object.exercise;

public abstract class Student extends Person{
    public Student() {
    }

    public Student(String name, String age) {
        super(name, age);
    }
    public abstract void study();
}
