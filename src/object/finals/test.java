package object.finals;

public class test {
    final int num = 10;
//    num=1; 常量不可以再修改
}

class  a  {
    // 不让重写
    public final void show() {
    }
}
// 不让继承
final class  b extends a {
}

//class c extends b {
//
//}