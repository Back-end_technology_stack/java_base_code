package object.extend.company;

public class Employe {
    String code_number;
    String user_name;
    int wages;
    public void eat () {
        System.out.println("吃米饭");
    }
    public void work () {
        System.out.println("工作");
    }
    public  Employe(){}
    public Employe(String code, String name, int wages) {
        this.code_number = code;
        this.user_name = name;
        this.wages = wages;
    }
}
