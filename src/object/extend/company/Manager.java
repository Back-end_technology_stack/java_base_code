package object.extend.company;

public class Manager extends Employe{
    int capital_management;
// 初始化自己的值
    public Manager() {
        // 如果测试类不给参数，我们需要自己给参数
        // 下面的方式二选一
        // this("10086","宋宇",190000);// 调用自己的构造函数
        super("8888","宋宇",5555555); // 掉父类的构造
        this.capital_management = 2000000;
    }
    public Manager(String code, String name, int wages, int capital) {
        super(code, name, wages);
        this.capital_management = capital;
    }
//    重写自己的方法

    @Override
    public void work() {
        System.out.println("管理其他人");
    }
}
