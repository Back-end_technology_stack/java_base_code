package object.codeblock;

public class code {
    // 构造代码块，优先于构造函数执行前执行
    {
        System.out.println("我是构造代码块");
    }

    private String name;

    public code(String name) {
        this.name = name;
    }

    protected void show() {
        {
            System.out.println("我是局部代码块");
        }
    }

    static {
        System.out.println("我是静态代码块，我只会执行一次，在类初始化的时候执行");
    }
}
