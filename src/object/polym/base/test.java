package object.polym.base;

public class test {
    public static void main(String[] args) {
        Student s = new Student();
        s.setAge(18);
        s.setName("小松");

        Teacher t = new Teacher();
        t.setAge(36);
        t.setName("王老师");

        Administrator a = new Administrator();
        a.setAge(40);
        a.setName("宋宇");

        registory(s);
        registory(t);
        registory(a);
    }
    public static void registory(Person obj) {
        obj.show();
    }
}
