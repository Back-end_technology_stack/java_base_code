package object.polym.zoon;

public class test {
    public static void main(String[] args) {
        Person p = new Person("老王",21);
        Dog d = new Dog(2,"黑色");
        p.keepPet(d,"骨头");

        Person p3 = new Person("老李",26);
        Cat c = new Cat(3,"白色");
        p3.keepPet(c,"🐟");
    }
}
