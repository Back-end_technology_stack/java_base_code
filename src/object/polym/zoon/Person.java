package object.polym.zoon;

public class Person {
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person() {
    }
    // 不使用函数重载的写法实现
//    public void keepPet(Dog dog, String something) {
//        System.out.println("年龄为："+age+"岁的"+name+"养了一个"+dog.getColor()+"颜色的"+dog.getAge()+"岁的狗");
//        dog.eat(something);
//    }
//
//    public void keepPet(Cat dog, String something) {
//        System.out.println("年龄为："+age+"岁的"+name+"养了一个"+dog.getColor()+"颜色的"+dog.getAge()+"岁的狗");
//        dog.eat(something);
//    }

    public void keepPet(Animal a, String something) {
        if (a instanceof Dog b) {
            System.out.println("年龄为："+age+"岁的"+name+"养了一个"+b.getColor()+"颜色的"+b.getAge()+"岁的狗");
            b.eat(something);
        }else if(a instanceof Cat c) {
            System.out.println("年龄为："+age+"岁的"+name+"养了一个"+c.getColor()+"颜色的"+c.getAge()+"岁的猫");
            c.eat(something);
        } else {
            System.out.println("不存在");
        }
    }
}
