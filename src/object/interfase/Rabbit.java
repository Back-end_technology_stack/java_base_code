package object.interfase;

public class Rabbit extends Animal{
    @Override
    public void eat() {
        System.out.println("兔子吃红烧肉");
    }

    public Rabbit() {
    }

    public Rabbit(String name, int age) {
        super(name, age);
    }
}
