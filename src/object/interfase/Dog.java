package object.interfase;

public class Dog extends Animal implements Swim{

    @Override
    public void eat() {
        System.out.println("狗吃耗子");
    }

    public Dog() {
    }

    public Dog(String name, int age) {
        super(name, age);
    }

    @Override
    public void swim() {
        System.out.println("狗会狗刨");
    }
}
