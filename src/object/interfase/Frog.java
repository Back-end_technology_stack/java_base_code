package object.interfase;

public class Frog extends Animal implements Swim{
    @Override
    public void eat() {
        System.out.println("青蛙吃狗");
    }

    public Frog() {
    }

    public Frog(String name, int age) {
        super(name, age);
    }

    @Override
    public void swim() {
        System.out.println("青蛙会游泳");
    }
}
