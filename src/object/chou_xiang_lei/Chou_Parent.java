package object.chou_xiang_lei;
// 抽象方法必须放在抽象类里，且类不可以被实例
public abstract class Chou_Parent {
    private String name;
    private int age;
    // 抽象方法
    public abstract void run ();
    public abstract void run (String name, int age);

    public Chou_Parent(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Chou_Parent() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
