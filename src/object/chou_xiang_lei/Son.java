package object.chou_xiang_lei;

public  class Son extends Chou_Parent{
    public Son(String name, int age) {
        super(name, age);
    }

    public Son() {
    }

    @Override
    public void run() {
        System.out.println("我叫："+getName()+"今年"+getAge()+"岁了");
    }
    @Override
    public void run(String name, int age) {
        setName(name);
        setAge(age);
        System.out.println("我叫："+getName()+"今年"+getAge()+"岁了");
    }
}
