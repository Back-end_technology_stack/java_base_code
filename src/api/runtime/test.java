package api.runtime;

import java.io.IOException;

public class test {
    public static void main(String[] args) throws IOException {
        // 获取Runtime对象
        Runtime r = Runtime.getRuntime();
        // 退出虚拟机
//        r.exit(0);
//        System.out.println("我不会执行了");
        // 获取cpu总线程数
        int n = r.availableProcessors();
        System.out.println(n);
        // 虚拟级从系统里获取最大内存分配
        long larg = r.maxMemory();
        System.out.println(larg/1024/1024);// mb
        // 已经得到的内存大小
        System.out.println(r.totalMemory()/1024/1024);
        // 剩余没有使用的
        System.out.println(r.freeMemory()/1024/1024);
        // 打开一个记事本
        r.exec("notepad");
    }
}
