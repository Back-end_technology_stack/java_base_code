package api.system;

public class test {
    public static void main(String[] args) {
        // 0:表示当前虚拟机是正常退出
        // 非0是异常退出
//        System.exit(0);
//        System.out.println("我会执行吗？");

        // 获取当前时间戳
       long i =  System.currentTimeMillis();
//        System.out.println(i);
    // 数组拷贝
        int[] arr1 = {1,2,3,4,5,6,7,8,9,10};
        int[] arr2 = new int[10];
        // 1:原数据数组
        // 2. 从数据源数组第几个位置开始拷贝
        //3.目标数组，要把数据拷入哪个数组
        //4. 目标数组的起始位置
        // 5. 拷贝的个数
        System.arraycopy(arr1,0,arr2,0,10);
        for (int j = 0; j < arr2.length; j++) {
            System.out.print(arr2[j]+" ");
        }
    }

}
